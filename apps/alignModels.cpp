/**
 * @file alignModels.cpp
 * @brief alignModels
 *
 * @author Abhijit Kundu
 */


#include "AlignCAD/AlignCADViewer.h"
#include "AlignCAD/CADInfo.h"
#include "AlignCAD/OrientedBox.h"

#include <QApplication>
#include <QFileDialog>
#include <QSettings>
#include <iostream>

int main(int argc, char** argv)
{
  // Read command lines arguments.
  QApplication application(argc,argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>();

  // Instantiate the viewers
  CuteGL::AlignCADViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());



  viewer.showAndWaitTillExposed();

  QSettings settings("AlignCADModels", "Align CAD Models App");
  QFileInfo cad_file = QFileDialog::getOpenFileName(0,
      QObject::tr("Open 3D Model"), settings.value("App/LastFileOpenDir").toString(),
      QObject::tr("3D Model Files (*.3ds *.ase *.dae *.xml *.blend *.obj *.x *.ply *.ifc *.nff *md2 *.md3 *.stl *.dxf *.lwo *.md5mesh *.xgl)"));

  // Save current folder for persistence
  if(cad_file.exists()) {
    QDir current_dir;
    settings.setValue("App/LastFileOpenDir", cad_file.absoluteFilePath());
  }
  else {
    std::cout << "Error: No valid file selected\n";
  }

  renderer->addModel(cad_file.absoluteFilePath().toStdString());


  // Run main loop.
  application.exec();

  CADInfo info;
  info.transform = renderer->modelPose().cast<double>();

  std::string cadinfo_path(cad_file.baseName().toStdString() + ".ini");
  writeCADInfo(info, cadinfo_path);
  std::cout << "Saved  CADInfo at " << cadinfo_path << "\n";

  std::cout << "Final Transformation = \n" << info.transform.matrix() << "\n";

  return EXIT_SUCCESS;
}

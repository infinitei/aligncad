/**
 * @file CADViewer.h
 * @brief CADViewer
 *
 * @author Abhijit Kundu
 */


#ifndef CADVIEWER_H_
#define CADVIEWER_H_

#include "AlignCAD/CADRenderer.h"
#include <CuteGL/Surface/WindowRenderViewer.h>


namespace CuteGL {

class CADViewer : public WindowRenderViewer {
 public:

  enum Action {
    NEXT_MODEL = 100,
    PREVIOUS_MODEL,
  };

  explicit CADViewer(CADRenderer* renderer);

 protected:
  virtual void handleKeyboardAction(int action);
  virtual void postDraw();

 protected:
  CADRenderer* renderer_;
};

}  // namespace CuteGL

#endif // end CADVIEWER_H_

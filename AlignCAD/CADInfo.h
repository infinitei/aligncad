/**
 * @file CADInfo.h
 * @brief CADConfig
 *
 * @author Abhijit Kundu
 */


#ifndef CAD_INFO_H_
#define CAD_INFO_H_

#include <Eigen/Geometry>

struct CADInfo {
  CADInfo()
      : transform(Eigen::Projective3d::Identity()),
        dimension(-1,-1,-1),
        class_name("Unknown"),
        instance_name("Unknown") {
  }

  Eigen::Projective3d transform;
  Eigen::Vector3d dimension;
  std::string class_name;
  std::string instance_name;
};


//TODO use boost property tree to handle all this


void writeCADInfo(const CADInfo& info, const std::string& filename);

CADInfo readCADInfo(const std::string& filename);

//CADInfo readMatCADInfo(const std::string& filename);

#endif // end CAD_INFO_H_

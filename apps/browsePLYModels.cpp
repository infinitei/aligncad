/**
 * @file browsePLYModels.cpp
 * @brief browsePLYModels
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADRenderer.h"
#include "AlignCAD/CADViewer.h"

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <CuteGL/Geometry/ComputeNormals.h>

#include <QGuiApplication>
#include <QFileInfo>
#include <iostream>
#include <limits>

void loadPlyModels(CuteGL::CADRenderer::ModelData& model_data, const std::vector<std::string>& cad_files) {
  model_data.resize(cad_files.size());
#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< cad_files.size(); ++i) {
    CuteGL::CADRenderer::ModelInfo& mi = model_data[i];

    std::get<QString>(mi) = QFileInfo(cad_files[i].c_str()).baseName();

    try {
      std::get<CuteGL::MeshData>(mi) = CuteGL::createFlatShadedMesh(CuteGL::loadMeshFromPLY(cad_files[i]));
    } catch (const std::exception& e) {
      std::cout << "Error Loading from file: " << cad_files[i] << "\n"
                << "Exception message: " << e.what() << "'\n";
    }

    std::get<Eigen::Affine3f>(mi) = Eigen::Affine3f::Identity();
  }
}

template <class Scalar>
bool isClose(Scalar a, Scalar b, Scalar tolerence = std::numeric_limits<Scalar>::epsilon()) {
    return std::abs(a - b) < tolerence;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "ERROR: Assets glob not provided" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/assets/folder/*.ply";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  // Get the input args in vector of strings
  std::vector<std::string> cad_files(argv + 1, argv + argc);

  // Read command lines arguments.
  int fake = 0;
  QGuiApplication application(fake, argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>();

  std::cout << "Loading " << cad_files.size() << " meshes ........... " << std::flush;
  loadPlyModels(renderer->modelData(), cad_files);
  std::cout << "Done" << std::endl;

  // Compute overall BBX
  Eigen::AlignedBox3f bbx_union;
  for (const auto& model_info : renderer->modelData()) {
    Eigen::AlignedBox3f bbx = computeAlignedBox(std::get<CuteGL::MeshData>(model_info));

    if (! isClose(bbx.diagonal().norm(), 1.0f, 1e-6f)) {
      std::cout << "bbx.diagonal() = " << bbx.diagonal().norm() << " for " <<  (std::get<QString>(model_info)).toStdString() << std::endl;
    }

    bbx_union.extend(bbx);
  }
  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "Union BBX: " << bbx_union.min().format(vecfmt) << " ---- " << bbx_union.max().format(vecfmt) << "\n";

  // Instantiate the viewers
  CuteGL::CADViewer viewer(renderer.get());

  viewer.setBackgroundColor(255, 255, 255);
  viewer.resize(1024, 768);

  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());


  viewer.showAndWaitTillExposed();

  renderer->showNextModel();

  // Run main loop.
  return application.exec();
}





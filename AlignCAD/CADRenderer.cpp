/**
 * @file CADRenderer.cpp
 * @brief CAD Scene
 *
 * @author Abhijit Kundu
 */

#include "CADRenderer.h"
#include "CADInfo.h"
#include <CuteGL/IO/ImportViaAssimp.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <iostream>
#include <QFileInfo>
#include <QDir>

namespace CuteGL {

CADRenderer::CADRenderer()
    : model_pose_(Eigen::Isometry3f::Identity()),
      cad_file_id_(-1) {
}

CADRenderer::CADRenderer(const ModelData& model_data)
  : model_pose_(Eigen::Isometry3f::Identity()),
    cad_file_id_(-1),
    model_data_(model_data) {
}

void CADRenderer::addModel(const std::string& filename, const Eigen::Affine3f& init_transform) {
  std::vector<MeshData> meshes = loadMeshesViaAssimp(filename);
  std::cout << "Loaded Model with " << meshes.size() << " meshes\n";
  std::cout << "# of Faces = " << numberOfFaces(meshes) << "  # of vertices = " << numberOfVertices(meshes) << "\n";

  addModel(meshes, init_transform);
}

void CADRenderer::addModel(const std::vector<MeshData>& meshes, const Eigen::Affine3f& init_transform) {
  model_drawer_.reset(new ModelDrawer(phongShader().program, meshes));
  model_pose_ = init_transform;

  model_vertices_.clear();
  for (const MeshData& md : meshes) {
    for (const MeshData::VertexData& vd : md.vertices) {
      model_vertices_.push_back(vd.position);
    }
  }
}

void CADRenderer::addModel(const MeshData& mesh, const Eigen::Affine3f& init_transform) {
  model_drawer_.reset(new ModelDrawer(phongShader().program, mesh));
  model_pose_ = init_transform;

  model_vertices_.clear();
  for (const MeshData::VertexData& vd : mesh.vertices) {
    model_vertices_.push_back(vd.position);
  }
}

void CADRenderer::init(PhongShader& shader) {
}

void CADRenderer::draw(PhongShader& shader) {
  if(model_drawer_) {
    shader.setModelPose(model_pose_);
    model_drawer_->draw();
  }

}

void CADRenderer::showNextModel() {
  // Save current pose for this model
  if(cad_file_id_ >= 0)
    std::get<PoseType>(model_data_[cad_file_id_]) = model_pose_;


  const int max_file_id = model_data_.size() - 1;
  if(cad_file_id_ < max_file_id) {
    ++cad_file_id_;

    const ModelInfo& model_info = model_data_[cad_file_id_];
    addModel(std::get<MeshData>(model_info), std::get<PoseType>(model_info));
  }
}

void CADRenderer::showPrevModel() {
  // Save current pose for this model
  if(cad_file_id_ >= 0)
    std::get<PoseType>(model_data_[cad_file_id_]) = model_pose_;


  if(cad_file_id_ > 0) {
    --cad_file_id_;

    const ModelInfo& model_info = model_data_[cad_file_id_];
    addModel(std::get<MeshData>(model_info), std::get<PoseType>(model_info));
  }
}

QString CADRenderer::modelName() const {
  QString name("Unknown");
  if(cad_file_id_ >= 0 && cad_file_id_ < (int)model_data_.size()) {
    name = QString("Model#%1 Name:%2").arg(QString::number(cad_file_id_), std::get<QString>(model_data_[cad_file_id_]));
  }
  return name;
}

}  // namespace CuteGL

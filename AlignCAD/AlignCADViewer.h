/**
 * @file AlignCADViewer.h
 * @brief AlignCADViewer
 *
 * @author Abhijit Kundu
 */

#ifndef ALIGNCADVIEWER_H_
#define ALIGNCADVIEWER_H_

#include "AlignCAD/CADViewer.h"

namespace CuteGL {

class AlignCADViewer : public CADViewer {

  enum Action {
    SET_ROTATE_X = 200,
    SET_ROTATE_Y,
    SET_ROTATE_Z,
    ROTATE90_CCW,
    ROTATE90_CW,
    ALIGN_BY_MV_BBX,
    ALIGN_BY_AA_BBX
  };


 public:
  explicit AlignCADViewer(CADRenderer* renderer);

 protected:
  virtual void handleKeyboardAction(int action);
  virtual void postDraw();

 private:
  int rotate_axis_;
};

}  // end namespace CuteGL
#endif // end ALIGNCADVIEWER_H_

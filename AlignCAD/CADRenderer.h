/**
 * @file CADRenderer.h
 * @brief ModelViewerScene
 *
 * @author Abhijit Kundu
 */

#ifndef CAD_RENDERER_H_
#define CAD_RENDERER_H_

#include <CuteGL/Renderer/BasicLightRenderer.h>
#include <CuteGL/Drawers/ModelDrawer.h>
#include <Eigen/StdVector>
#include <memory>
#include <tuple>

namespace CuteGL {

class CADRenderer : public BasicLightRenderer {
 public:
  using PositionType = MeshData::VertexData::PositionType;
  using PoseType = Eigen::Affine3f;

  using ModelInfo = std::tuple<QString, MeshData, PoseType>;
  using ModelData = std::vector<ModelInfo, Eigen::aligned_allocator<ModelInfo> >;

  CADRenderer();
  CADRenderer(const ModelData& model_data);

  void addModel(const std::string& filename,
                const PoseType& init_transform = PoseType::Identity());

  void addModel(const std::vector<MeshData>& meshes,
                const PoseType& init_transform = PoseType::Identity());

  void addModel(const MeshData& mesh,
                const PoseType& init_transform = PoseType::Identity());

  void showNextModel();
  void showPrevModel();

  PoseType& modelPose() {return model_pose_;}
  const PoseType& modelPose() const {return model_pose_;}

  const std::vector<PositionType>& modelVertices() const {return model_vertices_;}

  ModelData& modelData() {return model_data_;}
  const ModelData& modelData() const {return model_data_;}

  QString modelName() const;

 protected:
  virtual void init(PhongShader& shader);
  virtual void draw(PhongShader& shader);

 private:
  std::unique_ptr<ModelDrawer> model_drawer_;
  PoseType model_pose_;
  std::vector<PositionType> model_vertices_;

  int cad_file_id_;
  ModelData model_data_;

 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}  // namespace CuteGL

#endif // CAD_RENDERER_H_

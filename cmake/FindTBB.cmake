#######################################################
# Find Intel TBB.
#
# Defines the following variables:
#
#   TBB_FOUND             - Found the TBB framework
#   TBB_INCLUDE_DIRS      - Include directories
#   TBB_LIBRARIES         - TBB lib files
#   TBB_MALLOC_LIBRARIES  - TBB malloc libraries
#   TBB_VERSION           - TBB Version string
#
#######################################################
# Example Usage:
#
#    find_package(TBB REQUIRED)
#    include_directories(${TBB_INCLUDE_DIRS})
#
#    add_executable(foo foo.cc)
#    target_link_libraries(foo ${TBB_LIBRARIES})
#    
#    #if need to use tbb malloc then use:
#    target_link_libraries(foo ${TBB_LIBRARIES} ${TBB_MALLOC_LIBRARIES})
#
#######################################################
# If for some reason this fails to automatically find TBB, the easiest way
# is to set the TBB_ROOT_DIR folder
#######################################################

message( STATUS "Finding TBB ...")

find_path (TBB_ROOT_DIR
  NAMES include/tbb/tbb.h
  PATHS ENV TBBROOT
        ENV TBB40_INSTALL_DIR
        ENV TBB30_INSTALL_DIR
        ENV TBB22_INSTALL_DIR
        ENV TBB21_INSTALL_DIR
        ENV ICPP_COMPILER13
        ENV ICPP_COMPILER14
        "C:/Program Files/Intel/TBB"
        /opt/intel/tbb
  PATH_SUFFIXES tbb
  DOC "TBB root directory")

#Find TBB header files
find_path (TBB_INCLUDE_DIRS
  NAMES tbb/tbb.h
  PATHS 
    ${TBB_ROOT_DIR}/include
    /usr/include
    /usr/local/include
    ~/local/include
  DOC "TBB header file path"
  )
mark_as_advanced( TBB_INCLUDE_DIRS )

if (DEFINED TBB_COMPILER)
  set (_TBB_COMPILER ${TBB_COMPILER})
elseif (MSVC12)
  set (_TBB_COMPILER vc12)
elseif (MSVC11)
  set (_TBB_COMPILER vc11)
elseif (MSVC10)
  set (_TBB_COMPILER vc10)
elseif (MSVC90)
  set (_TBB_COMPILER vc9)
elseif (MSVC80)
  set (_TBB_COMPILER vc8)
elseif (MINGW)
  set (_TBB_COMPILER mingw)
elseif (WIN32)
  set (_TBB_COMPILER vc_mt)
elseif (UNIX)
  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.4.0")
    set (_TBB_COMPILER gcc4.1)
  else()
    set (_TBB_COMPILER gcc4.4)
  endif()
endif ()

# Determine between intel64 vs ia32 libs
if (CMAKE_SIZEOF_VOID_P EQUAL 8)
  set (_TBB_POSSIBLE_LIB_SUFFIXES lib/intel64/${_TBB_COMPILER})
else (CMAKE_SIZEOF_VOID_P EQUAL 8)
  set (_TBB_POSSIBLE_LIB_SUFFIXES lib/ia32/${_TBB_COMPILER})
endif (CMAKE_SIZEOF_VOID_P EQUAL 8)


set(_TBB_LIB_NAME "tbb")
set(_TBB_LIB_MALLOC_NAME "${_TBB_LIB_NAME}malloc")
set(_TBB_LIB_DEBUG_NAME "${_TBB_LIB_NAME}_debug")
set(_TBB_LIB_MALLOC_DEBUG_NAME "${_TBB_LIB_MALLOC_NAME}_debug")

#Find TBB lib files
find_library (TBB_LIBRARY
  NAMES ${_TBB_LIB_NAME}
  PATHS
    ${TBB_ROOT_DIR}
    ENV LIBRARY_PATH 
    ENV LD_LIBRARY_PATH
  PATH_SUFFIXES ${_TBB_POSSIBLE_LIB_SUFFIXES}
  DOC "TBB release library")

find_library (TBB_LIBRARY_DEBUG
  NAMES ${_TBB_LIB_DEBUG_NAME}
  HINTS ${TBB_ROOT_DIR}
  PATHS ENV LIBRARY_PATH 
        ENV LD_LIBRARY_PATH
  PATH_SUFFIXES ${_TBB_POSSIBLE_LIB_SUFFIXES}
  DOC "TBB Debug library")

find_library (TBB_MALLOC_LIBRARY
  NAMES ${_TBB_LIB_MALLOC_NAME}
  HINTS ${TBB_ROOT_DIR}
  PATHS ENV LIBRARY_PATH 
        ENV LD_LIBRARY_PATH
  PATH_SUFFIXES ${_TBB_POSSIBLE_LIB_SUFFIXES}
  DOC "TBB Malloc Library")

find_library (TBB_MALLOC_LIBRARY_DEBUG
  NAMES ${_TBB_LIB_MALLOC_DEBUG_NAME}
  HINTS ${TBB_ROOT_DIR}
  PATHS ENV LIBRARY_PATH 
        ENV LD_LIBRARY_PATH
  PATH_SUFFIXES ${_TBB_POSSIBLE_LIB_SUFFIXES}
  DOC "TBB Malloc Debug library")
   
mark_as_advanced( TBB_LIBRARY )
mark_as_advanced( TBB_MALLOC_LIBRARY )
mark_as_advanced( TBB_LIBRARY_DEBUG )
mark_as_advanced( TBB_MALLOC_LIBRARY_DEBUG )

if(TBB_LIBRARY)
  list(APPEND TBB_LIBRARIES "optimized")
  list(APPEND TBB_LIBRARIES "${TBB_LIBRARY}")
endif()
if(TBB_LIBRARY_DEBUG)
  list(APPEND TBB_LIBRARIES "debug")
  list(APPEND TBB_LIBRARIES "${TBB_LIBRARY_DEBUG}")
endif()

if(TBB_MALLOC_LIBRARY)
  list(APPEND TBB_MALLOC_LIBRARIES "optimized")
  list(APPEND TBB_MALLOC_LIBRARIES "${TBB_MALLOC_LIBRARY}")
endif()
if(TBB_MALLOC_LIBRARY_DEBUG)
  list(APPEND TBB_MALLOC_LIBRARIES "debug")
  list(APPEND TBB_MALLOC_LIBRARIES "${TBB_MALLOC_LIBRARY_DEBUG}")
endif()

#include the Standard package handler
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args( 
  TBB 
  DEFAULT_MSG 
  TBB_ROOT_DIR 
  TBB_INCLUDE_DIRS 
  TBB_LIBRARIES 
  TBB_MALLOC_LIBRARIES)

# Determine library's version
set (_TBB_VERSION_HEADER ${TBB_INCLUDE_DIRS}/tbb/tbb_stddef.h)
if (EXISTS ${_TBB_VERSION_HEADER})
  file (READ ${_TBB_VERSION_HEADER} _TBB_VERSION_CONTENTS)

  string (REGEX REPLACE ".*#define TBB_VERSION_MAJOR[ \t]+([0-9]+).*" "\\1"
    TBB_VERSION_MAJOR "${_TBB_VERSION_CONTENTS}")
  string (REGEX REPLACE ".*#define TBB_VERSION_MINOR[ \t]+([0-9]+).*" "\\1"
    TBB_VERSION_MINOR "${_TBB_VERSION_CONTENTS}")

  set (TBB_VERSION ${TBB_VERSION_MAJOR}.${TBB_VERSION_MINOR})
  set (TBB_VERSION_COMPONENTS 2)
endif ()
/**
 * @file simpleMeshModelViewer.cpp
 * @brief simpleMeshModelViewer
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADViewer.h"
#include "AlignCAD/CADRenderer.h"
#include <CuteGL/IO/ImportViaAssimp.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <QApplication>
#include <QFileDialog>
#include <QSettings>
#include <QKeyEvent>
#include <memory>
#include <iostream>

int main(int argc, char** argv)
{
  // Read command lines arguments.
  QApplication app(argc,argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>();


  // Instantiate the viewers
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());


  viewer.showAndWaitTillExposed();

  QSettings settings("SimpleMeshModelViewer", "Simple Mesh Model Viewer");
  QString qfileName = QFileDialog::getOpenFileName(0,
      QObject::tr("Open 3D Model"), settings.value("App/LastFileOpenDir").toString(),
      QObject::tr("3D Model Files (*.3ds *.ase *.dae *.xml *.blend *.obj *.x *.ply *.ifc *.nff *md2 *.md3 *.stl *.dxf *.lwo *.md5mesh *.xgl)"));

  // Save current folder for persistence
  if(!qfileName.isEmpty()) {
    QDir current_dir;
    settings.setValue("App/LastFileOpenDir", current_dir.absoluteFilePath(qfileName));
  }

  std::vector<CuteGL::MeshData> meshes = CuteGL::loadMeshesViaAssimp(qfileName.toStdString());
  std::cout << "Loaded Model with " << meshes.size() << " meshes\n";
  std::cout << "# of Faces = " << numberOfFaces(meshes) << "  # of vertices = " << numberOfVertices(meshes) << "\n";

  Eigen::AlignedBox3f bbx = computeAlignedBox(meshes);
  std::cout << bbx.min().transpose() << "\n";
  std::cout << bbx.max().transpose() << "\n";


  // Translate and scale to center and fit
  Eigen::Affine3f tfm = Eigen::UniformScaling<float>(
      1.0f / bbx.diagonal().norm()) * Eigen::Translation3f(-bbx.center());

  renderer->addModel(meshes, tfm);

  // Run main loop.
  app.exec();
  return EXIT_SUCCESS;
}

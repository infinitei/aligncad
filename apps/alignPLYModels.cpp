/**
 * @file alignPLYModels.cpp
 * @brief alignPLYModels
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/AlignCADViewer.h"
#include "AlignCAD/CADInfo.h"

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <QApplication>
#include <QFileInfo>
#include <iostream>

CuteGL::CADRenderer::ModelData loadPlyModels(const std::vector<std::string>& cad_files) {
  CuteGL::CADRenderer::ModelData model_data(cad_files.size());
  std::cout << "Loading " << cad_files.size() << " meshes ........... " << std::flush;

#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< cad_files.size(); ++i) {
    CuteGL::CADRenderer::ModelInfo& mi = model_data[i];

    std::get<QString>(mi) = QFileInfo(cad_files[i].c_str()).baseName();

    try {
      std::get<CuteGL::MeshData>(mi) = CuteGL::loadMeshFromPLY(cad_files[i]);
    } catch (const std::exception& e) {
      std::cout << "Error Loading from file: " << cad_files[i] << "\n"
                << "Exception message: " << e.what() << "'\n";
    }

    const Eigen::AlignedBox3f aabb = computeAlignedBox(std::get<CuteGL::MeshData>(mi));
    Eigen::Isometry3f wTo = Eigen::Isometry3f::Identity();
    wTo.translation() = aabb.center();
    const float scale = 1. / aabb.diagonal().norm();
    const Eigen::Isometry3f oTw = wTo.inverse();
    std::get<Eigen::Affine3f>(mi) = Eigen::UniformScaling<float>(scale) * oTw;

  }
  std::cout << "Done" << std::endl;

  return model_data;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "ERROR: Assets glob not provided" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/assets/folder/*.ply";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  // Get the input args in vector of strings
  std::vector<std::string> cad_files(argv + 1, argv + argc);

  // Read command lines arguments.
  int fake = 0;
  QApplication application(fake, argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>(loadPlyModels(cad_files));

  Eigen::AlignedBox3f bbx;
  for (const auto& model_info : renderer->modelData()) {
    bbx.extend(computeAlignedBox(std::get<CuteGL::MeshData>(model_info)));
  }
  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "BBX: " << bbx.min().format(vecfmt) << " ---- " << bbx.max().format(vecfmt) << "\n";

  // Instantiate the viewers
  CuteGL::AlignCADViewer viewer(renderer.get());

  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());


  viewer.showAndWaitTillExposed();

  renderer->showNextModel();

  // Run main loop.
  application.exec();

  if (renderer->modelData().size() != cad_files.size()) {
    std::cout << "Something weird. renderer->modelData().size() != cad_files.size()\n";
    return EXIT_FAILURE;
  }

  for (size_t i = 0; i< cad_files.size(); ++i) {
    const CuteGL::CADRenderer::ModelInfo& mi = renderer->modelData()[i];
    CADInfo info;
    info.transform = std::get<Eigen::Affine3f>(mi).cast<double>();

    QFileInfo cad_file(cad_files[i].c_str());
    const QString cadinfo_file = cad_file.path() + "/" + cad_file.completeBaseName() + ".ini";
    if (std::get<QString>(mi) != cad_file.completeBaseName())
      throw std::runtime_error("bad filename");

    writeCADInfo(info, cadinfo_file.toStdString());
    std::cout << "Saved  CADInfo at " << cadinfo_file.toStdString() << "\n";
  }

  return EXIT_SUCCESS;
}




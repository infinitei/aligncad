/**
 * @file CADViewer.cpp
 * @brief CADViewer
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADViewer.h"
#include <QPainter>

namespace CuteGL {

CADViewer::CADViewer(CADRenderer* renderer)
    : WindowRenderViewer(renderer),
      renderer_(renderer) {

  keyboard_handler_.registerKey(NEXT_MODEL, Qt::Key_Right, "Move to next frame");
  keyboard_handler_.registerKey(PREVIOUS_MODEL, Qt::Key_Left, "Move to previous frame");
}

void CADViewer::handleKeyboardAction(int action) {
  switch (action) {
    case NEXT_MODEL:
    {
      renderer_->showNextModel();
      break;
    }
    case PREVIOUS_MODEL:
    {
      renderer_->showPrevModel();
      break;
    }
    default:
      WindowRenderViewer::handleKeyboardAction(action);
      break;
  }
}

void CADViewer::postDraw() {
  WindowRenderViewer::postDraw();
  glfuncs_->glDisable(GL_DEPTH_TEST);
  QPainter p(this);
  p.setPen(foregroundColor());
  p.drawText(20, 20, renderer_->modelName());
  p.end();
  glfuncs_->glEnable(GL_DEPTH_TEST);
}

}  // namespace CuteGL

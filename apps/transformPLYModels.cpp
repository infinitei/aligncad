/**
 * @file transformPLYModels.cpp
 * @brief transform PLY Models based on CADInfo
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADInfo.h"
#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <QFileInfo>
#include <QDir>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;

  if (argc < 2) {
    std::cout << "ERROR: Asset folder path not provided and/or glob not provided" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/assets/folder/*ply";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  std::vector<std::string> cad_files(argv + 1, argv + argc);
  std::cout << "cad_files.size() = " << cad_files.size() << "\n";
  if (cad_files.size() == 0) {
    return EXIT_FAILURE;
  }

  for (const std::string& cad_file : cad_files) {
    std::cout << "\n---------------------------------------------\n";

    QFileInfo cad_fi(cad_file.c_str());

    if (!cad_fi.exists()) {
      std::cout << "ERROR: cad file does not exist\n";
      continue;
    }

    std::cout << "Working on " << cad_fi.fileName().toStdString() << std::endl;

    QFileInfo cad_config_fi;
    cad_config_fi.setFile(cad_fi.absoluteDir(), cad_fi.baseName() + QString(".ini"));

    if (!cad_config_fi.exists()) {
      std::cout << "ERROR: cad_config_file does not exist\n";
      continue;
    }

    CADInfo ci = readCADInfo(cad_config_fi.absoluteFilePath().toStdString());
    Eigen::Affine3f tfm(ci.transform.matrix().cast<float>());

    MeshData mesh = loadMeshFromPLY(cad_file);
    transformMesh(mesh, tfm);
    colorizeMesh(mesh, MeshData::ColorType(192, 192, 192, 255));


    std::string output_file = cad_fi.baseName().toStdString() + ".ply";
    saveMeshAsBinPly(mesh, output_file);

    std::cout << "Saved transformed mesh as " << output_file << std::endl;
  }

  return EXIT_SUCCESS;
}





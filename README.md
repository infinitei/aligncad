# AlignCAD #

Align 3D CAD models.

This repo has tools to allign, view, and browse 3D CAD models.

### Dependencies ###

 - [CMake] - a cross-platform, open-source build system
 - [CuteGL] - Simple OpenGL visualization library based on Qt and Eigen
 - [Eigen] - a C++ template library for linear algebra 
 - [ApproxMVBB] - Approximate minimal volume oriented bounding box of 3D point cloud **[optional]**


### Installation ###

You can then compile and build using the cmake build system. For an Unix system, that might look as the following steps:

1. Clone the repo
2. `cd /path/to/repo`
4. `mkdir build`
5. `cd build`
6. `cmake ..`
7. `make -j6`


*Author: [Abhijit Kundu]*

  [CuteGL]: https://bitbucket.org/infinitei/CuteGL
  [Eigen]: http://eigen.tuxfamily.org/
  [CMake]: http://www.cmake.org/
  [Doxygen]: http://doxygen.org
  [Abhijit Kundu]: http://abhijitkundu.info/
  [ApproxMVBB]: https://github.com/gabyx/ApproxMVBB
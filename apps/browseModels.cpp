/**
 * @file browseModels.cpp
 * @brief browseModels
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADRenderer.h"
#include "AlignCAD/CADViewer.h"

#include <CuteGL/IO/ImportViaAssimp.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <QGuiApplication>
#include <QFileInfo>
#include <iostream>

CuteGL::CADRenderer::ModelData loadModels(const std::vector<std::string>& cad_files) {
  CuteGL::CADRenderer::ModelData model_data(cad_files.size());
  std::cout << "Loading " << cad_files.size() << " meshes ........... " << std::endl;

#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< cad_files.size(); ++i) {
    CuteGL::CADRenderer::ModelInfo& mi = model_data[i];

    const QFileInfo fi(cad_files[i].c_str());

    std::cout << "Loading Mesh " << fi.absoluteFilePath().toStdString() << " ........... " << std::flush;

    std::get<QString>(mi) = fi.baseName();
    std::get<CuteGL::MeshData>(mi) = CuteGL::joinMeshes(CuteGL::loadMeshesViaAssimp(cad_files[i]));
    std::get<Eigen::Affine3f>(mi) = Eigen::Affine3f::Identity();

    std::cout << "Done" << std::endl;
  }

  return model_data;
}

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "ERROR: Asset folder path not provided and/or glob not provided" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/assets/folder/*.{3ds,ply}";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  std::vector<std::string> cad_files(argv + 1, argv + argc);
  std::cout << "cad_files.size() = " << cad_files.size() << "\n";
  if(cad_files.size() == 0) {
    return EXIT_FAILURE;
  }

  // Read command lines arguments.
  int fake = 0;
  QGuiApplication application(fake, argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>(loadModels(cad_files));

  // Compute overall BBX
  Eigen::AlignedBox3f bbx;
  for (const auto& model_info : renderer->modelData()) {
    bbx.extend(computeAlignedBox(std::get<CuteGL::MeshData>(model_info)));
  }
  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "Union BBX: " << bbx.min().format(vecfmt) << " ---- " << bbx.max().format(vecfmt) << "\n";

  // Instantiate the viewers
  CuteGL::CADViewer viewer(renderer.get());

  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f),
                           Eigen::Vector3f::Zero(),
                           Eigen::Vector3f::UnitZ());


  viewer.showAndWaitTillExposed();

  renderer->showNextModel();

  // Run main loop.
  return application.exec();
}





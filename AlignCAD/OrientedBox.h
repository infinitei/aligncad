/**
 * @file OrientedBox.h
 * @brief OrientedBox
 *
 * @author Abhijit Kundu
 */


#ifndef EIGEN_ORIENTED_BOX_H_
#define EIGEN_ORIENTED_BOX_H_

#include <Eigen/Geometry>

namespace Eigen {

template <class Scalar_, int _AmbientDim, class _OrientationType = Eigen::Transform<Scalar_ , _AmbientDim, Eigen::AffineCompact> >
class OrientedBox : public Eigen::AlignedBox<Scalar_, _AmbientDim> {
 public:
  typedef Eigen::AlignedBox<Scalar_, _AmbientDim> AlignedBoxType;
  static const int  NumberofCornerPoints = 1 << _AmbientDim;
  typedef Eigen::Matrix<Scalar_, _AmbientDim, NumberofCornerPoints> CornerPointsMatrixType;
  typedef _OrientationType OrientationType;

  /// Default constructor initializing a null box.
  inline OrientedBox() : AlignedBoxType(), orientation_(OrientationType::Identity()) { }

  template<typename OtherVectorType1, typename OtherVectorType2>
  OrientedBox(const OtherVectorType1& min,
              const OtherVectorType2& max)
      : AlignedBoxType(min, max),
        orientation_(OrientationType::Identity()) {
  }

  template<typename OtherVectorType1, typename OtherVectorType2, typename OtherOrientation>
  OrientedBox(const OtherVectorType1& min,
              const OtherVectorType2& max,
              const OtherOrientation& tfm)
      : AlignedBoxType(min, max),
        orientation_(tfm) {
  }

  CornerPointsMatrixType cornerPointsMatrix() const {
    CornerPointsMatrixType corners;
    for (int j = 0; j < NumberofCornerPoints; ++j) {
      for (int i = 0; i < _AmbientDim; ++i) {
        corners(i, j) = (j / (1 << i)) % 2 ? this->m_max[i] : this->m_min[i];
      }
    }
    return orientation_ * corners;
  }

  const OrientationType& orientation() const {return orientation_;}
  OrientationType& orientation() {return orientation_;}

  /** \returns true if the point \a p is inside the box \c *this. */
  template<typename Derived>
  inline bool contains(const Eigen::MatrixBase<Derived>& a_p) const {
    return AlignedBoxType::contains(orientation_.inverse() * a_p);
  }

protected:
  OrientationType orientation_;
};


typedef OrientedBox<float, 3> OrientedBox3f;
typedef OrientedBox<double, 3> OrientedBox3d;

}  // namespace Eigen

#endif // end EIGEN_ORIENTED_BOX_H_

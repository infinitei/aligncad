/**
 * @file makeModelsUnitDiagonal.cpp
 * @brief makeModelsUnitDiagonal
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <iostream>
#include <boost/filesystem.hpp>

void makeUnitDiagonalAndSave(const std::vector<std::string>& cad_files) {
  std::cout << "Loading " << cad_files.size() << " meshes ........... " << std::flush;

#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< cad_files.size(); ++i) {
    CuteGL::MeshData md;
    try {
      md = CuteGL::loadMeshFromPLY(cad_files[i]);
    } catch (const std::exception& e) {
      std::cout << "Error Loading from file: " << cad_files[i] << "\n"
                << "Exception message: " << e.what() << "'\n";
    }

    const Eigen::AlignedBox3f aabb = CuteGL::computeAlignedBox(md);
    Eigen::Isometry3f wTo = Eigen::Isometry3f::Identity();
    wTo.translation() = aabb.center();
    const float scale = 1. / aabb.diagonal().norm();
    const Eigen::Isometry3f oTw = wTo.inverse();
    Eigen::Affine3f tfm = Eigen::UniformScaling<float>(scale) * oTw;

    CuteGL::transformMesh(md, tfm);

    boost::filesystem::path p(cad_files[i]);

    CuteGL::saveMeshAsBinPly(md, p.filename().string());

  }
  std::cout << "Done" << std::endl;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "ERROR: Assets glob not provided" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/assets/folder/*.ply";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  // Get the input args in vector of strings
  std::vector<std::string> cad_files(argv + 1, argv + argc);

  makeUnitDiagonalAndSave(cad_files);


  return EXIT_SUCCESS;
}

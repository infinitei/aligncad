/**
 * @file renderPLYModelFromViewPoint.cpp
 * @brief renderPLYModelFromViewPoint
 *
 * @author Abhijit Kundu
 */

#include "AlignCAD/CADRenderer.h"
#include "AlignCAD/CADViewer.h"

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <CuteGL/Geometry/ComputeNormals.h>

#include <boost/program_options.hpp>
#include <QGuiApplication>
#include <QFileInfo>
#include <iostream>
#include <cmath>

void loadPlyModels(CuteGL::CADRenderer::ModelData& model_data, const std::vector<std::string>& cad_files) {
  model_data.resize(cad_files.size());
#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< cad_files.size(); ++i) {
    CuteGL::CADRenderer::ModelInfo& mi = model_data[i];

    std::get<QString>(mi) = QFileInfo(cad_files[i].c_str()).baseName();

    try {
      std::get<CuteGL::MeshData>(mi) = CuteGL::createFlatShadedMesh(CuteGL::loadMeshFromPLY(cad_files[i]));
    } catch (const std::exception& e) {
      std::cout << "Error Loading from file: " << cad_files[i] << "\n"
                << "Exception message: " << e.what() << "'\n";
    }

    std::get<Eigen::Affine3f>(mi) = Eigen::Affine3f::Identity();
  }
}


Eigen::Vector3f spherical2cartesian(float azimuth_deg, float elevation_deg, float radial) {
  const float pi_by_180 = M_PI / 180.0f;
  const float azimuth_rad = azimuth_deg * pi_by_180;
  const float elevation_rad = elevation_deg * pi_by_180;
  Eigen::Vector3f cart;
  cart.x() = (radial * std::cos(elevation_rad) * std::cos(azimuth_rad));
  cart.y() = (radial * std::cos(elevation_rad) * std::sin(azimuth_rad));
  cart.z() = (radial * std::sin(elevation_rad));
  return cart;
}

int main(int argc, char **argv) {

  float azimuth, elevation, tilt, distance;

  namespace po = boost::program_options;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
    config_options.add_options()
        ("cad_files",  po::value<std::vector<std::string>>(), "Path to CAD files")
        ("azimuth,a",  po::value<float>(&azimuth)->default_value(0.0f), "Azimuth in Degrees)")
        ("elevation,e",  po::value<float>(&elevation)->default_value(0.0f), "Elevation in Degrees)")
        ("tilt,t",  po::value<float>(&tilt)->default_value(0.0f), "Tilt in Degrees)")
        ("distance,d",  po::value<float>(&distance)->default_value(3.0f), "Distance to object center)")
        ;

  po::positional_options_description p;
  p.add("cad_files", -1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);

    if (!vm.count("cad_files"))
      throw po::error("Please provide at-least one mesh file");
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  // Get the input args in vector of strings
  const std::vector<std::string> cad_files = vm["cad_files"].as<std::vector<std::string>>();

  // Read command lines arguments.
  int fake = 0;
  QGuiApplication application(fake, argv);

  // Create the renderer
  std::unique_ptr<CuteGL::CADRenderer> renderer = std::make_unique<CuteGL::CADRenderer>();

  std::cout << "Loading " << cad_files.size() << " meshes ........... " << std::flush;
  loadPlyModels(renderer->modelData(), cad_files);
  std::cout << "Done" << std::endl;

  // Compute overall BBX
  Eigen::AlignedBox3f bbx;
  for (const auto& model_info : renderer->modelData()) {
    bbx.extend(computeAlignedBox(std::get<CuteGL::MeshData>(model_info)));
  }
  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "Union BBX: " << bbx.min().format(vecfmt) << " ---- " << bbx.max().format(vecfmt) << "\n";

  // Instantiate the viewers
  CuteGL::CADViewer viewer(renderer.get());

  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(960, 540);

  QSurfaceFormat format = viewer.format();
  format.setSamples(16);
  viewer.setFormat(format);


  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(1050.0f, 1050.0f, 0.0f, 480.0f, 270.0f, 960, 540, 0.1f, 100.0f);



  // Compute extrinsics from viewpoint
  const float pi_by_180 = M_PI / 180.0f;
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromViewPoint(
      azimuth * pi_by_180,
      elevation * pi_by_180,
      tilt * pi_by_180,
      distance);

  viewer.showAndWaitTillExposed();

  renderer->showNextModel();

  // Run main loop.
  return application.exec();
}





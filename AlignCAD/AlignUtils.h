/**
 * @file AlignUtils.h
 * @brief AlignUtils
 *
 * @author Abhijit Kundu
 */

#ifndef ALIGN_UTILS_H_
#define ALIGN_UTILS_H_

#include "AlignCAD/Config.h"
#include <Eigen/Geometry>


#ifdef WITH_ApproxMVBB

#include <ApproxMVBB/ComputeApproxMVBB.hpp>


/// Get Alignement Transform using Min Volume oriented Bounding Box
template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Affine>
getMVBBtransformation(const std::vector<Eigen::Matrix<Scalar, 3, 1> >& vertices) {


  ApproxMVBB::Matrix3Dyn w_points(3, vertices.size());
  for (Eigen::Index i = 0; i < w_points.cols(); ++i) {
    w_points.col(i) = vertices[i].template cast<double>();
  }
//  //Alternatively Map to Eigen Point Cloud
//  Eigen::Map<const ApproxMVBB::Matrix3Dyn> w_points(vertices[0].data(), 3, vertices.size());

  const unsigned int num_of_samples = std::min(20000U, static_cast<unsigned int>(vertices.size()));
  ApproxMVBB::OOBB oobb = ApproxMVBB::approximateMVBB(w_points, 0.0001, num_of_samples, 5, 0, 5);
  //  ApproxMVBB::OOBB oobb = ApproxMVBB::approximateMVBBDiam(points,0.001);  // faster

  // world_T_bbx
  Eigen::Isometry3d wTb = Eigen::Isometry3d::Identity();
  wTb.linear() = oobb.m_q_KI.matrix();

  // Make all points inside the OOBB
  Eigen::Matrix3d bTw_R = wTb.inverse().linear();
  for (Eigen::DenseIndex i = 0; i < w_points.cols(); ++i) {
    oobb.unite(bTw_R * w_points.col(i));
  }

  // bbx_T_object
  Eigen::Isometry3d bTo = Eigen::Isometry3d::Identity();
  bTo.translation() = oobb.center();

  // Scaling
  double scale = 1. / oobb.maxExtent();

  // Main isometry tfm
  Eigen::Isometry3d oTw = bTo.inverse() * wTb.inverse();


  Eigen::Affine3d scaled_oTw = Eigen::UniformScaling<double>(scale) * oTw;

  return scaled_oTw.cast<Scalar>();
}

#endif

/// Get Alignement Transform using Axis Aligned Bounding Box
template<typename Scalar>
Eigen::Transform<Scalar, 3, Eigen::Affine>
getAABBtransformation(const std::vector<Eigen::Matrix<Scalar, 3, 1> >& vertices) {
  using Vector3 = Eigen::Matrix<Scalar, 3, 1>;
  using Affine3 = Eigen::Transform<Scalar, 3, Eigen::Affine>;
  using Isometry3 = Eigen::Transform<Scalar, 3, Eigen::Isometry>;
  using AlignedBox3 = Eigen::AlignedBox<Scalar, 3>;

  // Make all points inside the OOBB
  AlignedBox3 aabb;
  for (const Vector3& vertex : vertices) {
    aabb.extend(vertex);
  }

  // world_T_object
  Isometry3 wTo = Isometry3::Identity();
  wTo.translation() = aabb.center();

  // Scaling
  Scalar scale = 1. / aabb.diagonal().norm();

  // Main isometry tfm
  Isometry3 oTw = wTo.inverse();

  Affine3 out = Eigen::UniformScaling<Scalar>(scale) * oTw;
  return out;
}


#endif // end ALIGN_UTILS_H_

/**
 * @file CADInfo.cpp
 * @brief CADInfo
 *
 * @author Abhijit Kundu
 */

#include "CADInfo.h"
#include <fstream>
#include <stdexcept>

void writeCADInfo(const CADInfo& info, const std::string& filename) {

  std::ofstream outfile(filename);
  if (!outfile.is_open()) {
    throw std::runtime_error("ERROR: Saving Poses File");
  }

  Eigen::IOFormat fmt(Eigen::FullPrecision, Eigen::DontAlignCols, " ", " ");

  outfile << "###------- CAD Info -----------###" << "\n";

  outfile << "Transform = " << info.transform.matrix().format(fmt) << "\n";

  outfile << "Dimension = " << info.dimension.format(fmt) << "\n";;

  outfile << "InstanceName = " << info.instance_name << "\n";

  outfile << "ClassName = " << info.class_name << "\n";

  outfile.close();
}

CADInfo readCADInfo(const std::string& filename) {
  std::ifstream infile(filename);
  if (!infile.is_open()) {
    throw std::runtime_error("ERROR: Opening CAD Info file FAILED");
  }
  CADInfo info;

  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);

    std::string token;
    iss >> token;

    // Check for comment lines
    if(token[0] == '#')
      continue;

    std::string equal_sign;
    iss >> equal_sign; // equal sign
    if(equal_sign != "=")
      throw std::runtime_error("ERROR: Unexpected token");

    if (token == "Transform") {
      Eigen::Matrix4d temp;
      iss >> temp(0, 0) >> temp(0, 1) >> temp(0, 2) >> temp(0, 3)
          >> temp(1, 0) >> temp(1, 1) >> temp(1, 2) >> temp(1, 3)
          >> temp(2, 0) >> temp(2, 1) >> temp(2, 2) >> temp(2, 3)
          >> temp(3, 0) >> temp(3, 1) >> temp(3, 2) >> temp(3, 3);
      info.transform.matrix() = temp;
    } else if (token == "Dimension") {
      iss >> info.dimension(0) >> info.dimension(1) >> info.dimension(2);
    } else if (token == "ClassName") {
      iss >> info.class_name;
    } else if (token == "InstanceName") {
      iss >> info.instance_name;
    }
  }

  infile.close();
  return info;
}



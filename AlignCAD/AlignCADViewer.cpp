/**
 * @file AlignCADViewer.cpp
 * @brief AlignCADViewer
 *
 * @author Abhijit Kundu
 */

#define _USE_MATH_DEFINES
#include <cmath>

#include "AlignCAD/AlignCADViewer.h"
#include "AlignCAD/AlignUtils.h"
#include <QPainter>



namespace CuteGL {

AlignCADViewer::AlignCADViewer(CADRenderer* renderer)
    : CADViewer(renderer),
      rotate_axis_(2) {

  keyboard_handler_.registerKey(SET_ROTATE_X, Qt::Key_X, "Set Rotation around X");
  keyboard_handler_.registerKey(SET_ROTATE_Y, Qt::Key_Y, "Set Rotation around Y");
  keyboard_handler_.registerKey(SET_ROTATE_Z, Qt::Key_Z, "Set Rotation around Z");

  keyboard_handler_.registerKey(ROTATE90_CCW, Qt::Key_Up, "Set Rotation around Y");
  keyboard_handler_.registerKey(ROTATE90_CW, Qt::Key_Down, "Set Rotation around Z");

  keyboard_handler_.registerKey(ALIGN_BY_MV_BBX, Qt::SHIFT + Qt::Key_O, "Align by Min Vol BBX");
  keyboard_handler_.registerKey(ALIGN_BY_AA_BBX, Qt::SHIFT + Qt::Key_A, "Align by Axis Aligned BBX");
}

void AlignCADViewer::handleKeyboardAction(int action) {
  switch (action) {
    case SET_ROTATE_X:
    {
      rotate_axis_ = 0;
      break;
    }
    case SET_ROTATE_Y:
    {
      rotate_axis_ = 1;
      break;
    }
    case SET_ROTATE_Z:
    {
      rotate_axis_ = 2;
      break;
    }
    case ROTATE90_CCW:
    {
      Eigen::AngleAxisd rot(0.5 * M_PI, Eigen::Vector3d::Unit(rotate_axis_));
      renderer_->modelPose() = rot.cast<float>() * renderer_->modelPose();
      break;
    }
    case ROTATE90_CW:
    {
      Eigen::AngleAxisd rot(-0.5 * M_PI, Eigen::Vector3d::Unit(rotate_axis_));
      renderer_->modelPose() = rot.cast<float>() * renderer_->modelPose();
      break;
    }
    case ALIGN_BY_MV_BBX:
    {
#ifdef WITH_ApproxMVBB
      renderer_->modelPose() = getMVBBtransformation(renderer_->modelVertices());
#endif
      break;
    }
    case ALIGN_BY_AA_BBX:
    {
      renderer_->modelPose() = getAABBtransformation(renderer_->modelVertices());
      break;
    }
    default:
      CADViewer::handleKeyboardAction(action);
      break;
  }
}

void AlignCADViewer::postDraw() {
  CADViewer::postDraw();
  glfuncs_->glDisable(GL_DEPTH_TEST);
  QPainter p(this);
  p.setPen(foregroundColor());

  QString rot_direction;
  switch (rotate_axis_) {
    case 0:
      rot_direction = "ROTATION: X";
      break;
    case 1:
      rot_direction = "ROTATION: Y";
      break;
    case 2:
      rot_direction = "ROTATION: Z";
      break;
    default:
      rot_direction = "ROTATION: BAD_DIRECTION";
      break;
  }

  p.drawText(20, 50, rot_direction);
  p.end();
  glfuncs_->glEnable(GL_DEPTH_TEST);
}

}  // end namespace CuteGL
